import React from 'react';

export default function Question3(props) {
    return(
        <div>
            <div>
                <TextInput type={props.entity.entityId.type} placeholder={props.entity.entityId.placeholder} onChange={(value) => props.onChangeEntityId(value)}></TextInput>
            </div>
            <div>
                <TextInput type={props.entity.entityName.type} placeholder={props.entity.entityName.placeholder} onChange={(value) => props.onChangeEntityName(value)}></TextInput>
            </div>
            <div>
                <TextInput type={props.entity.price.type} placeholder={props.entity.price.placeholder} onChange={(value) => props.price.onChangePrice(value)}></TextInput>
            </div>
            <div>
                <TextInput type={props.entity.status.type} placeholder={props.entity.status.placeholder} onChange={(value) => props.onChangeStatus(value)}></TextInput>
            </div>
        </div>
    )
}

const TextInput = (props) => {
    return (
        <div>
            <input type={props.type} placeholder={props.placeholder} onChange={(event) => props.onChange(event.target.value)} />
        </div>
    );
};