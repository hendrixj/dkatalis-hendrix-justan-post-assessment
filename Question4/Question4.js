import React from 'react'
import Question3 from './Question3';
import Question1 from './Question1';
import { throwStatement } from '@babel/types';

export default class Question4 extends React.Component{
    constructor(props){
        super(props);

        let entity={
            "entityId": {
                "type": "text",
                "placeholder": "Entity ID"
            },
            "entityName": {
                "type": "text",
                "placeholder": "Entity Name"
            },
            "price": {
                "type": "number",
                "placeholder": "Price"
            },
            "status": {
                "type": "number",
                "placeholder": "Status"
            }
        }

        this.state = {
            "entityId": "",
            "entityName": "",
            "price": "",
            "status": "",
            "entityList": []
        }

        this.setEntityId = this.setEntityId.bind(this);
        this.setEntityName = this.setEntityName.bind(this);
        this.setPrice = this.setPrice.bind(this);
        this.setStatus = this.setStatus.bind(this);
        this.addToList = this.addToList.bind(this);
    }

    setEntityId(value){
        this.setState({
            entityId: value
        })
    }
    setEntityName(value){
        this.setState({
            entityName: value
        })
    }
    setPrice(value){
        this.setState({
            price: value
        })
    }
    setStatus(value){
        this.setState({
            status: value
        })
    }

    addToList(){
        let dataList = this.state.entityList
        let data = {
            "entityId": this.state.entityId,
            "entityName": this.state.entityName,
            "price": this.state.price,
            "status": this.state.status
        }
        dataList.push(data)
        this.setState({
            entityList: dataList
        })
        
    }  


    render(){
        return(
            <div>
                <Question3 entity={this.entity} onChangeEntityId={this.setEntityId} onChangeEntityName={this.setEntityName}
                    onChangePrice={this.setPrice} onChangeStatus={this.setStatus} ></Question3>
                <button onClick={(event)=> this.addToList()}> Add</button>
                <Question1 entityList={this.state.entityList}></Question1>
            </div>
        )
    }
}