import React from "react";

export default function Question2 (props){
    return (
        <div>
            {
                props.entityList.map((field) => {
                let classNameStatus = "";

                if(field.status === 0)
                    classNameStatus = "red"
                else if(field.status === 1)
                    classNameStatus = "orange"
                else
                    classNameStatus = "green"
                return (
                    <div>
                    <div>
                        {field.entityId}
                    </div>
                    <div>
                        {field.entityName}
                    </div>
                    <div>
                        {field.price}
                    </div>
                    <div className={classNameStatus}>
                        {field.status}
                    </div>
                    </div>)
                })
            }   
        </div>
    )
}